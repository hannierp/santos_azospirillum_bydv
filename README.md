# Code Repository for the Article:  
**"The plant growth-promoting rhizobacterium Azospirillum brasilense reduces symptoms and aphid population growth on wheat plants infected with Barley yellow dwarf virus"**

This repository contains the R script necessary to replicate the data analysis presented in the publication.

## Usage
To replicate the data analysis, follow these steps:

1. **Run the R script**:  
   Use the script provided in `./R/azospirillum_bydv_manuscript_code.R`. This script contains all the necessary code and functions to reproduce the analyses.

2. **Access the source data**:  
   The source data required for the analysis is stored in `./data/azospirillum_bydv_SourceData.RData`.

## Publication
**authors et al.**  
*"The plant growth-promoting rhizobacterium Azospirillum brasilense reduces symptoms and aphid population growth on wheat plants infected with Barley yellow dwarf virus"*  
Proceedings of the Royal Society B: Biological Sciences (2024). In review.  
DOI: https://doi.org/xxx

